package shadattonmoy.sustnavigator.utils;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ThrowOnExtraProperties;
import com.google.firebase.database.ValueEventListener;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import shadattonmoy.sustnavigator.Course;
import shadattonmoy.sustnavigator.MainActivity;
import shadattonmoy.sustnavigator.R;
import shadattonmoy.sustnavigator.SplashScreen;
import shadattonmoy.sustnavigator.admin.model.Admin;
import shadattonmoy.sustnavigator.admin.view.AdminPanelFragment;

public class Values {
    public static String[] months = new String[]{"January","February","March","April","May","June","July","August","September","October","November","December"};
    public static String[] depts = new String[]{"FES","ARC","CEP","CEE","CSE","EEE","FET","IPE","MEE","PME","SWE","BMB","GEB","BAN","CHE","GEE","MAT","OCG","PHY","STA","ANP","BNG","ECO","ENG","PSS","PAD","SCW","SCO"};
    public static String[] days = new String[]{"Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"};
    public static final int DETECTED_TYPE_COURSE_CODE = 1;
    public static final int DETECTED_TYPE_COURSE_TITLE = 2;
    public static final int DETECTED_TYPE_COURSE_CREDIT = 3;
    public static final int VIEW_TYPE_TEXTVIEW = 1;
    public static final int VIEW_TYPE_EDITTEXT = 2;
    public static final int REQUEST_CODE_SIGN_IN_FOR_BACKUP = 1;
    public static final int REQUEST_CODE_SIGN_IN_FOR_RESTORE = 2;
    public static final int CONTACT_FOR_SYLLABUS = 1;
    public static final int CONTACT_FOR_FACULTY = 2;
    public static final int CONTACT_FOR_HOLIDAY = 3;
    public static final int CONTACT_FOR_PROCTOR = 4;
    public static final int CONTACT_FOR_STAFF = 5;
    public static final int CONTACT_FOR_SYLLABUS_DETAILS= 6;
    public static final String GITHUB_LINK = "https://github.com/Shadat-tonmoy/SUST-Navigator";
    public static final String DATABASE_NAME = "sust_nav_database";
    public static final String SHARED_PREF_NAME = "app_shared_pref";
    public static final String DEV_GITHUB_LINK = "https://github.com/Shadat-tonmoy/";
    public static final String DEV_FB_LINK = "https://www.facebook.com/shadat.tonmoy";
    public static final String DEV_LINKEDIN_LINK = "https://www.linkedin.com/in/shadat-tonmoy-06266b27/";
    public static final String PURPOSE_SYLLABUS_MANAGE = "syllabus_manage";
    public static final String PURPOSE_STAFF_MANAGE = "staff_manage";
    public static final String PURPOSE_TEACHER_MANAGE = "teacher_manage";
    public static final String DEFAULT_SESSION = "default_session";
    public static final String ADMIN__APPREOVED = "admin_approved";
    public static final String DEMO_SYLLABUS = "Demo Syllabus (2014-15 Session)";
    public static final String DEMO_SYLLABUS_SESSION = "2014-15";
    public static Admin LOGGED_IN_ADMIN = null;
    public static boolean LOGIN_TIME = false;





    public static boolean IS_LOCAL_ADMIN = false;
    private static Map<String,String> semesterCodeMap = new HashMap<>();
    public static List<String> getSessions()
    {
        List<String> sessions = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        sessions.add((year-1)+"-"+(year%100));
        sessions.add((year-2)+"-"+((year-1)%100));
        sessions.add((year-3)+"-"+((year-2)%100));
        sessions.add((year-4)+"-"+((year-3)%100));
        sessions.add((year-5)+"-"+((year-4))%100);
        sessions.add((year-6)+"-"+((year-5))%100);
        sessions.add(DEMO_SYLLABUS);
        return sessions;
    }

    public static Map getSemesterCodeMap()
    {
        semesterCodeMap.put("1st Year 1st Semester","1_1");
        semesterCodeMap.put("1st Year 2nd Semester","1_2");
        semesterCodeMap.put("2nd Year 1st Semester","2_1");
        semesterCodeMap.put("2nd Year 2nd Semester","2_2");
        semesterCodeMap.put("3rd Year 1st Semester","3_1");
        semesterCodeMap.put("3rd Year 2nd Semester","3_2");
        semesterCodeMap.put("4th Year 1st Semester","4_1");
        semesterCodeMap.put("4th Year 2nd Semester","4_2");
        semesterCodeMap.put("5th Year 1st Semester","5_1");
        semesterCodeMap.put("5th Year 2nd Semester","5_2");
        semesterCodeMap.put("Optionals","o_p");
        semesterCodeMap.put("Second Major","s_m");

        return semesterCodeMap;
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void updateLastModified()
    {
        final FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        String email = user.getEmail();
        Log.e("LoggedInAs",email);
        Query queryRef = databaseReference.child("admin").orderByChild("email").equalTo(email);
        queryRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot child : dataSnapshot.getChildren() )
                {
                    Admin admin = child.getValue(Admin.class);
                    String pushId = child.getKey();
                    admin.setId(pushId);
                    String name = admin.getName();
                    String regNo = admin.getRegNo();
                    String dept = admin.getDept();
                    long time = new Date().getTime();
                    LastModified lastModified = new LastModified(name,regNo,dept,time);
                    DatabaseReference databaseReference = firebaseDatabase.getReference().child("lastModified");
                    databaseReference.setValue(lastModified);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }





    public static void getAdminRequest(final Context context)
    {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        Query queryRef = databaseReference.child("admin").orderByChild("varified").equalTo(false);
        queryRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Admin> admins = new ArrayList<>();
                for(DataSnapshot child : dataSnapshot.getChildren() )
                {
                    Admin admin = child.getValue(Admin.class);
                    String pushId = child.getKey();
                    admin.setId(pushId);
                    String name = admin.getName();
                    String regNo = admin.getRegNo();
                    String dept = admin.getDept();
                    if(!admin.isVarified())
                    {
                        admins.add(admin);
                    }
                }
                if(admins.size()>0)
                {
                    AdminPanelFragment.setAdminReqMessage(admins.size()+" Admin Requests are pending for approval");

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    public static void getCurrentAdmin()
    {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if(user!=null)
        {
            String email = user.getEmail();
            Query queryRef = databaseReference.child("admin").orderByChild("email").equalTo(email);
            queryRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                        Admin admin = child.getValue(Admin.class);
                        Values.LOGGED_IN_ADMIN = admin;
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
    }

    public static String getTimeString(long timeStamp)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeStamp);
        return calendar.getTime().toString();
    }

    public static String getSemesterCode(String semester)
    {

        return semester.replace("/","_").toLowerCase();
    }

    public static void openLink(Context context,String url)
    {
        Uri uri = Uri.parse(url); // missing 'http://' will cause crashed
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        context.startActivity(intent);
    }

    public static void showToast(Context context,String msg)
    {
        Toast.makeText(context,msg,Toast.LENGTH_SHORT).show();
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static String trimStringTo(String src,int len)
    {
        if(src.length()>len)
        {
            src = src.substring(0,len-3) + "..";
        }
        return src;

    }

    public static void restartApp(Context context) {
        Intent refresh = new Intent(context, SplashScreen.class);
        refresh.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(refresh);
    }

    public static String getEmailForSyllabus(String adminName,String dept,String session)
    {
        String message = "Hello "+adminName+", Please update the Syllabus Record for "+dept+" Dept of "+session+" of SUST Navigator App. Its an emergency. We people are facing trouble with the current data. Thanks";
        return message;
    }

    public static String getEmailForSyllabusDetail(String adminName,String dept,String session,Course course)
    {
        String message = "Hello "+adminName+", Please update the Syllabus Detail for "+dept+" Dept of "+session+" For the Course "+course.getCourse_code()+" - "+course.getCourse_title()+" of SUST Navigator App. Its an emergency. We people are facing trouble with the current data. Thanks";
        return message;
    }

    public static String getEmailForFaculty(String adminName,String dept)
    {
        String message = "Hello "+adminName+", Please update the Faculty Record for "+dept+" Dept of SUST Navigator App. Its an emergency. We people are facing trouble with the current data. Thanks";
        return message;
    }

    public static String getEmailForStaff(String adminName,String dept)
    {
        String message = "Hello "+adminName+", Please update the Staff Record for "+dept+" Dept of SUST Navigator App. Its an emergency. We people are facing trouble with the current data. Thanks";
        return message;
    }

    public static String getEmailForHoliday(String adminName,int holidayYear)
    {
        String message = "Hello "+adminName+", Please update the Holiday Record for Academic Year "+holidayYear+" of SUST Navigator App. Its an emergency. We people are facing trouble with the current data. Thanks";
        return message;
    }

    public static String getEmailForProctor(String adminName)
    {
        String message = "Hello "+adminName+", Please update the Proctorial Body Record of SUST Navigator App. Its an emergency. We people are facing trouble with the current data. Thanks";
        return message;
    }

    public static void sendEmail(String email,String message,Context context)
    {
        if(email==null || email.length()<4 )
        {
            Values.showToast(context,"Email Address not Available");
            return;
        }
        String[] address = {email};
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setData(Uri.parse("mailto:"));
        intent.setType("plain/text");
        intent.putExtra(android.content.Intent.EXTRA_EMAIL, email);
        intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "SUST Navigator Data Update");
        intent.putExtra(android.content.Intent.EXTRA_TEXT, message);
        intent.putExtra(Intent.EXTRA_EMAIL, address);
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        }
        else
        {
            Values.showToast(context,"Email App Not Found!!!");
        }
    }

    public static void startRateUsActivity(Context context)
    {
        Uri uri = Uri.parse("market://details?id=" + context.getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            context.startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            context.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + context.getPackageName())));
        }

    }

    public static void startShareAppActivity(Context context)
    {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String appURL = "https://play.google.com/store/apps/details?id="+context.getPackageName();
        String shareBody = "Hello SUSTIAN!! I would like to share SUST Navigator App With you. It's awsome. You can try it. Get it from "+appURL;
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        context.startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }

    public static void checkFirebaseConnected(Context context)
    {
        DatabaseReference connectedRef = FirebaseDatabase.getInstance().getReference(".info/connected");
        connectedRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                boolean connected = snapshot.getValue(Boolean.class);
                if (connected) {
                    showToast(context,"Connected");
                } else {
                    showToast(context,"Could not connect to db. Please enable the network connection");
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                System.err.println("Listener was cancelled");
            }
        });
    }

    public static String getHashedPassword(String password)
    {
        try {
            // Static getInstance method is called with hashing MD5
            MessageDigest md = MessageDigest.getInstance("MD5");
            // digest() method is called to calculate message digest
            //  of an input digest() return array of byte
            byte[] messageDigest = md.digest(password.getBytes());
            // Convert byte array into signum representation
            BigInteger no = new BigInteger(1, messageDigest);
            // Convert message digest into hex value
            String hashtext = no.toString(16);
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        }
        // For specifying wrong message digest algorithms
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    public static void setDefaultSession(Context context,String defaultSession)
    {
        SharedPreferences prefs = context.getSharedPreferences(
                SHARED_PREF_NAME, Context.MODE_PRIVATE);
        prefs.edit().putString(DEFAULT_SESSION, defaultSession).apply();
    }

    public static String getDefaultSession(Context context)
    {
        SharedPreferences prefs = context.getSharedPreferences(
                SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return prefs.getString(DEFAULT_SESSION, getSessions().get(0));
    }

    public static void setApprovalDone(Context context)
    {
        SharedPreferences prefs = context.getSharedPreferences(
                SHARED_PREF_NAME, Context.MODE_PRIVATE);
        prefs.edit().putBoolean(ADMIN__APPREOVED, true).apply();
    }

    public static boolean getApprovalDone(Context context)
    {
        SharedPreferences prefs = context.getSharedPreferences(
                SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return prefs.getBoolean(ADMIN__APPREOVED, false);
    }

    public static int getDefaultSessionIndex(Context context)
    {
        String defaultSession = getDefaultSession(context);
        int i=0;
        Log.e("Dafault",defaultSession);
        for(String session:getSessions())
        {
            if(session.equals(defaultSession))
                return i;
            i++;
        }
        return i;
    }

    public static String getDeviceID(Context context)
    {
        try {
            return Settings.Secure.getString(context.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
        }catch (Exception e)
        {
            return "unknownDeviceID";
        }

    }


}
