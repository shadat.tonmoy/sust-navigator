package shadattonmoy.sustnavigator.admin.model;

public class AdminRequest {
    private boolean isApproved = false;

    public AdminRequest() {
    }

    public AdminRequest(boolean isApproved) {
        this.isApproved = isApproved;
    }

    public boolean isApproved() {
        return isApproved;
    }

    public void setApproved(boolean approved) {
        isApproved = approved;
    }
}
