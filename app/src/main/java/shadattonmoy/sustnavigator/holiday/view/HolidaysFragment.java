package shadattonmoy.sustnavigator.holiday.view;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;

import shadattonmoy.sustnavigator.HolidayAddFragment;
import shadattonmoy.sustnavigator.commons.view.AdminListBottomSheet;
import shadattonmoy.sustnavigator.holiday.controller.HolidayAdapter;
import shadattonmoy.sustnavigator.R;
import shadattonmoy.sustnavigator.holiday.model.Holiday;
import shadattonmoy.sustnavigator.teacher.model.Teacher;
import shadattonmoy.sustnavigator.utils.Values;

/**
 * Created by Shadat Tonmoy on 8/31/2017.
 */

public class HolidaysFragment extends android.app.Fragment {


    private TextView holidayName,holidayDate, holidayDays,holidayTitle,noHolidayFoundView;
    private ImageView noHolidayFoundImage;
    private ArrayList<Holiday> holidays;
    ListView holidayList;
    private int year=-1;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;
    private ProgressBar progressBar;
    private FragmentManager manager;
    private Context context;
    private AppBarLayout appBarLayout;
    private FloatingActionButton addHolidayFab;
    private boolean isAdmin;
    private View view;
    private FragmentActivity activity;
    private LinearLayout noNetMessage;
    private boolean connected = false;


    public HolidaysFragment() {

    }

    public HolidaysFragment(boolean isAdmin) {
        this.isAdmin = isAdmin;

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        this.activity = (FragmentActivity) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view =inflater.inflate(R.layout.fragment_holidays, container, false);
        holidayTitle = (TextView) view.findViewById(R.id.holiday_title);
        noHolidayFoundView = (TextView) view.findViewById(R.id.nothing_found_txt);
        holidayName = (TextView)view.findViewById(R.id.holiday_name);
        holidayDate = (TextView)view.findViewById(R.id.holiday_date);
        holidayDays = (TextView)view.findViewById(R.id.holiday_days);
        holidayList = (ListView)view.findViewById(R.id.holiday_list);
        progressBar = (ProgressBar) view.findViewById(R.id.holiday_loading);
        noHolidayFoundImage = (ImageView) view.findViewById(R.id.nothing_found_image);
        appBarLayout = (AppBarLayout) getActivity().findViewById(R.id.appbar_layout);
        addHolidayFab = (FloatingActionButton) view.findViewById(R.id.add_holiday_fab);
        noNetMessage = view.findViewById(R.id.no_net_message);
        context = getActivity();
        activity = (FragmentActivity) getActivity();

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Calendar calendar = Calendar.getInstance();
        manager = getFragmentManager();
        year = calendar.get(Calendar.YEAR);
        holidayTitle.setText("Holidays of "+year);
        progressBar.setVisibility(View.VISIBLE);
        holidays = new ArrayList<Holiday>();
        noHolidayFoundView.setVisibility(View.GONE);
        appBarLayout.setExpanded(false);
        setHasOptionsMenu(true);
        getDataFromServer();
        checkForConnectionWithDB();
    }

    private void checkForConnectionWithDB()
    {
        new Thread(new Runnable() {
            public void run() {
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(holidays.size()==0  && !connected && !Values.isNetworkAvailable(context))
                        {
                            showNoInternetMessagge();
                        }
                    }
                });
            }
        }).start();
    }

    private void showNoInternetMessagge()
    {
//        Values.showToast(context,"No Internet Connection");
        progressBar.setVisibility(View.GONE);
        noNetMessage.setVisibility(View.VISIBLE);
    }

    private void getDataFromServer()
    {
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference().child("holiday").child(String.valueOf(year));
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int numOfHolidays = 0;
                holidays = new ArrayList<>();
                for(DataSnapshot child : dataSnapshot.getChildren())
                {
                    String key = child.getKey();
                    Holiday holiday = child.getValue(Holiday.class);
                    holiday.setHoliayId(key);
                    Log.e("HolidayTime",holiday.getStartTimeStamp()+"");
                    holidays.add(holiday);
                    numOfHolidays++;
                }
                connected =true;
                if(numOfHolidays==0)
                {
                    if(isAdmin)
                        noHolidayFoundView.setText(Html.fromHtml("Sorry!! No Holiday found for "+year+". <b>Tap '+' to add a new Holiday</b>"));
                    else {
                        noHolidayFoundView.setText(Html.fromHtml("Sorry!! No Holiday found for "+year+". Please <b>Contact Admin</b>"));
                        noHolidayFoundView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                requestAdmin();
                            }
                        });
                    }
                    try{
                        Glide.with(context).load(context.getResources()
                                .getIdentifier("nothing_found", "drawable", context.getPackageName())).thumbnail(0.5f)
                                .crossFade()
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .into(noHolidayFoundImage);
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                    noHolidayFoundView.setVisibility(View.VISIBLE);
                    noHolidayFoundImage.setVisibility(View.VISIBLE);

                }
                else
                {
                    sortByDate();
                    HolidayAdapter adapter = new HolidayAdapter(context,R.layout.holiday_single_row,R.id.holiday_desc,holidays,isAdmin,manager,view);
                    holidayList.setAdapter(adapter);
                    adapter.setActivity(getActivity());
                }
                progressBar.setVisibility(View.GONE);
                if(isAdmin)
                {
                    addHolidayFab.setVisibility(View.VISIBLE);
                    addHolidayFab.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            FragmentManager manager = getFragmentManager();
                            FragmentTransaction transaction = manager.beginTransaction();
                            HolidayAddFragment holidayAddFragment = new HolidayAddFragment(String.valueOf(year));
                            transaction.replace(R.id.main_content_root,holidayAddFragment);
                            transaction.addToBackStack("holiday_add_fragment");
                            transaction.commit();
                        }
                    });
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.admin_request_menu, menu);
        MenuItem item = menu.findItem(R.id.request_admin_menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.request_admin_menu:
                requestAdmin();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void sortByDate()
    {
        Collections.sort(holidays, new Comparator<Holiday>() {
            @Override
            public int compare(Holiday o1, Holiday o2) {
                long time1 = o1.getStartTimeStamp();
                long time2 = o2.getStartTimeStamp();
                Log.e("SortingBy",time1+" "+time2);
                if(time1>time2)
                    return 1;
                else return -1;
            }
        });
    }
    
    private void requestAdmin()
    {
        try {
            AdminListBottomSheet adminListBottomSheet = new AdminListBottomSheet();
            Bundle args = new Bundle();
            args.putInt("purpose",Values.CONTACT_FOR_HOLIDAY);
            adminListBottomSheet.setArguments(args);
            if(activity==null)
                activity = (FragmentActivity) getActivity();
            adminListBottomSheet.show(activity.getSupportFragmentManager(),"adminList");
        }
        catch (Exception e)
        {
            Values.showToast(context,"Sorry!! An error occurred");
        }

    }
}
